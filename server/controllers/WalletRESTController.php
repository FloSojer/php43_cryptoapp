<?php

require_once('RESTController.php');
require_once('models/Wallet.php');
require_once('models/WalletDetail.php');
require_once('models/Purchase.php');

class WalletRESTController extends RESTController
{

    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }

    private function handleGETRequest()
    {
        if($this->verb == null && sizeof($this->args) == 0){    //api/wallet
            $model = WalletDetail::getAllWalletsWithPurchaseSummary();
            $this->response($model);

        }else if($this->verb == null && sizeof($this->args) == 1){  //api/wallet/2
            $model = WalletDetail::getWalletWithPurchaseSummaryByWalletId($this->args[0]);
            if($model == null){
                $this->response("Not found", 404);
            }else{
                $this->response($model);
            }

        }else if($this->verb == null && sizeof($this->args) == 2){  //api/wallet/2/purchase
            if($this->args[1] != "purchase"){
                $this->response("Not found", 404);
            }else{
                $model = Purchase::getAllPurchasesByWallet($this->args[0]);
                $this->response($model);
            }
        }
        else if($this->verb != null && sizeof($this->args) == 1){
            $model = Purchase::getAllGroupByCurrency($this->args[0]);
            $this->response($model);
        }
        else{
            $this->response("Bad request", 400);
        }
    }
}