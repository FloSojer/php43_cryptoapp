CREATE TABLE `wallet` (
                          `id` int(11) NOT NULL,
                          `name` varchar(255) NOT NULL,
                          `currency` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `wallet`
--

INSERT INTO `wallet` (`id`, `name`, `currency`) VALUES
(1, 'bitcoinWallet', 'BTC'),
(2, 'ethereumWallet', 'ETH'),
(3, 'USDollar', 'USDT'),
(4, 'BedNBreakfast', 'BNB');