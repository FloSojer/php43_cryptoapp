<?php

require_once ('DatabaseObject.php');

class Wallet implements DatabaseObject, JsonSerializable
{
    protected $id;
    protected $name;
    protected $currency;

    protected $errors = [];

    public function __construct(){
    }

    public static function newWallet($id, $name, $currency){
        $wallet = new Wallet();
        $wallet->id = $id;
        $wallet->name = $name;
        $wallet->currency = $currency;
        return $wallet;
    }

    public function jsonSerialize()
    {
        return [
            "id" => intval($this->id),
            "name" =>$this->name,
            "currency" => $this->currency,
        ];
    }

    public function validate()
    {
        return $this->validateName() & $this->validateCurrency();
    }

    private function validateCurrency() {
        if (strlen($this->currency) == 0) {
            $this->errors['currency'] = "Waehrung ungueltig";
            return false;
        } else if (strlen($this->currency) > 4) {
            $this->errors['currency'] = "Waehrung zu lang (max. 4 Zeichen)";
            return false;
        } else {
            unset($this->errors['currency']);
            return true;
        }
    }

    private function validateName()
    {
        if (strlen($this->name) == 0) {
            $this->errors['name'] = "Name ungueltig";
            return false;
        } else if (strlen($this->name) > 30) {
            $this->errors['name'] = "Name zu lang (max. 30 Zeichen)";
            return false;
        } else {
            unset($this->errors['name']);
            return true;
        }
    }

    /**
     * create or update an object
     * @return boolean true on success
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }
            return true;
        }
        return false;
    }

    public function create()
    {
        // TODO: Implement create() method.
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM wallet WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));

        $walletDB = $stmt->fetch();

        $wallet = Wallet::newWallet($walletDB['id'], $walletDB['name'], $walletDB['currency']);

        return $wallet;
    }

    public static function getAll()
    {
        $db = Database::connect();
        $sql = "SELECT * FROM wallet";
        $stmt = $db->prepare($sql);
        $stmt->execute();

        $allWalletsDB = $stmt->fetchAll();

        $allWallets = [];

        foreach ($allWalletsDB as $walletDB){
            $allWallets[] = Wallet::newWallet($walletDB['id'], $walletDB['name'], $walletDB['currency']);
        }

        return $allWallets;
    }

    public static function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }


}

