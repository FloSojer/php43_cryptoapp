<?php

class WalletDetail extends Wallet implements JsonSerializable, DatabaseObject
{
    private $amount;
    private $price;

    public function __construct($id, $name, $currency, $amount, $price)
{
    $this->id = $id;
    $this->name = $name;
    $this->currency = $currency;
    $this->amount = $amount;
    $this->price = $price;
}

    //Überschreiben der jsonSerialize-Methode
    public function jsonSerialize()
{
    return [
        "id" => intval($this->id),
    "name" =>$this->name,
    "currency" => $this->currency,
    "amount" => doubleval($this->amount),
    "price" => doubleval($this->price),
];
}

    public static function getWalletWithPurchaseSummaryByWalletId($walletId){

    $wallet = Wallet::get($walletId);   //Erhalte Wallet-Objekt

    $walletName = $wallet->getName();
    $walletCurrency = $wallet->getCurrency();

    $amountSum = self::getAllPurchaseAmountSumByWallet($walletId);  //Erhalte Menge an Währungs-Stückzahl

    $priceSum = self::getAllPurchasePriceSumByWallet($walletId);  //Erhalte die Summe aller Kaufpreise

    $walletDetail = new WalletDetail($walletId, $walletName, $walletCurrency, $amountSum, $priceSum);   //Erstelle WalletDetail-Objekt

    return $walletDetail;
}

    public static function getAllWalletsWithPurchaseSummary(){

    $allWallets = Wallet::getAll();

    $allWalletDetails = []; //Erstelle Array allWalletDetails, indem alle WalletDetails-Objekte gepeichert werden

    //Erhalte für jede Währung die Summe an Währungs-Stückzahlen und die Summe aller Kaufpreise
    foreach ($allWallets as $wallet){

        $walletId = $wallet->getId();
        $walletName = $wallet->getName();
        $walletCurrency = $wallet->getCurrency();

        $amountSum = self::getAllPurchaseAmountSumByWallet($walletId);  //Erhalte Menge an Währungs-Stückzahl

        $priceSum = self::getAllPurchasePriceSumByWallet($walletId);  //Erhalte die Summe aller Kaufpreise

        //Erstelle neues WalletDetail-Objekt und füge es zum Array allWalletDetails hinzu.
        $allWalletDetails[] = new WalletDetail($walletId, $walletName, $walletCurrency, $amountSum, $priceSum);
    }

    return $allWalletDetails;
}

    //Funktion zum Erhalten der Summe aller Kaufpreise einer Währung
    private static function getAllPurchasePriceSumByWallet($walletId){
    $db = Database::connect();
    $sql = "SELECT amount, price FROM purchaseswithwalletattributes WHERE wallet_id = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($walletId));

    $allPurchasesByCurrencyDB = $stmt->fetchAll();

    $priceSum = 0;  //Variable für Gesamt-Preis

    foreach ($allPurchasesByCurrencyDB as $pruchaseByCurrency){
        $amount = $pruchaseByCurrency['amount'];
        $price = $pruchaseByCurrency['price'];

        $priceSum += $amount * $price; //einen einzelnen Preis aus Datensatz berechnen und dem Gesamtpreis hinzufügen
    }

    return $priceSum;
}

    //Funktion zum Erhalten der Stückzahl einer Währung
    private static function getAllPurchaseAmountSumByWallet($walletId){
    $db = Database::connect();
    $sql = "SELECT sum(amount) AS 'amount_sum' FROM purchaseswithwalletattributes GROUP BY wallet_id HAVING wallet_id = ?";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($walletId));

    $amountSumDB = $stmt->fetch();
    $amountSum = 0; //Stückzahl-Summe mit 0 initialisieren

    if(isset($amountSumDB['amount_sum'])) { //Erhalte Stückzahlen vom Wallet nur dann, wenn Stückzahlen vorhanden sind, sonst setze amountSum auf 0
        $amountSum = $amountSumDB['amount_sum'];
    }

    return $amountSum;
}

    /**
     * @return mixed
     */
    public function getAmount()
{
    return $this->amount;
}

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
{
    $this->amount = $amount;
}

    /**
     * @return mixed
     */
    public function getPrice()
{
    return $this->price;
}

    /**
     * @param mixed $price
     */
    public function setPrice($price)
{
    $this->price = $price;
}

}