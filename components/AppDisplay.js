app.component('app-display', {
    props: {

        purchases: Array,
        wallets: Array,
        prices: {},

    },
    template:
    /*html*/
        `
        <div
        class="wallet-display "
        >
        <div class="wallet-container">
         <purchase-form :prices = "prices" :wallets="wallets" @add-to-wallet="addToWallet"></purchase-form>
         <wallet-list :purchases="purchases" ></wallet-list>
        
        
        
</div>
        
        
        </div>
   `,
    data() {
        return {
        }
    },
    methods: {

    }
})
