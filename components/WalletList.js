

app.component('wallet-list', {


    props: {

        purchases: Array,
        test: Number
    },
    template:
    /*html*/
        `
       <div class="wallet-list">
       
       <h3>Wallets: {{getTotal()}}</h3>
       
       <ul id = "component">
       
             <li v-for="wallet in getWallets()">
             
             {{wallet.amount.toFixed(3) + " " + wallet.currency + " " +wallet.price + " €"}}
                 
             </li>
      
       </ul>
       
       
       </div>
    `,
    data: function () {
        return {
            amount: 0
        }
    },

    methods: {

        getWallets(){

            var list = [];
            var m = null;
            var ges = 0;
            var ar = this.$props.purchases;

            ar.forEach(element => {

                for(var i = 0 ; i < list.length ; i++ )
                {
                    if (list[i].currency == element.currency) {

                        m = i;
                        break;

                    }else{

                        m = null;

                    }
                }

                if(m == null){

                    list.push({amount: element.amount ,currency: element.currency , price: element.price })
                    ges = ges + element.price;

                }else{

                    list[m].price = list[m].price + element.price;
                    list[m].amount = list[m].amount + element.amount;
                    ges = ges + element.price;

                }

            });
            this.amount = ges;
            console.log(ges);

            return list;

        },

        getTotal(){

            var ges = 0;
            var ar = this.$props.purchases;

            ar.forEach(element => {

                    ges = ges + element.price;

            });
            console.log(ges);

            return ges.toFixed(2) + " €";

        }
    }
})

