app.component('purchase-form', {
    props: {
        prices: {},
        wallets: {
            type: Array,
            required: true
        },
    },
    template:
    /*html*/
        `
        <form class="wallet-form ">
        
        <h3>Cryptowärung kaufen: </h3>
        <label for = "currency" >Cryptowärung:</label>
        <select id="currency" name="currency" >
        <option v-for="obj in toShowCurrency()" v-bind:value="[obj.wert, obj.kuerzel]">{{obj.kuerzel}}, {{obj.wert}} EUR </option>
        </select>
        <label for = "wallet" >Wallet:</label> 
         
        <select id="wallet" name="wallet"> 
        <option v-for="wall in toShowWallets()"> {{wall.name}} </option>
        </select> 
        <label for = "amount" >Menge:</label>
       <input type="number" 
        id = "amount"
        name = "amount"
        step="any"
        value="0"
        required = required
        /> 
       <!-- <vue-numeric-input id = "amount" :value="0" controls-type="plusminus" width="60px" required= required ></vue-numeric-input> -->
        <script> 
        document.getElementById("amount").addEventListener('change', function () {
            
            let obj = document.getElementById("currency").value;
            console.log(obj);
            let wert = parseFloat(obj);
            console.log(wert);
            let menge = document.getElementById("amount").value;
            console.log(menge);
            var price = wert * menge;
            document.getElementById("Wert").innerHTML = price + ' €';
        })
        document.getElementById("currency").addEventListener('change', function () {
            
            let obj = document.getElementById("currency").value;
            console.log('Ganzes Objekt' + obj);
            let wert = parseFloat(obj);
            console.log('ganzer Wert' + wert);
            let menge = document.getElementById("amount").value;
            console.log(menge);
            var price = wert * menge;
            document.getElementById("Wert").innerHTML = price + ' €';
        })
        </script>
        Wert: <p id="Wert"> </p>
        
        <button class="button" v-on:click="buy()">Kaufen</button> <br>
       <!-- <button class="button" v-on:click="toShowCurrency()"> load Data</button>-->
        
        </form>
        
  `,
    methods: {

        toShowCurrency() {
            var list = [];
            //let date = this.getDate();
            //console.log(' Current Date: ' + date);
            //this.info = Vue.prototype.$bitpandaData;

            let obj = this.$props.prices.data;
            let arrOfKeys = Object.keys(obj);
            for (var i = 0; i < arrOfKeys.length; i++) {
                var kuerz = arrOfKeys[i];
                //console.log('WErt ' + obj[kuerz].EUR);
                var wertNotFixed = parseFloat(obj[kuerz].EUR);
                var wert = wertNotFixed.toFixed(2);
                //console.log('Kuerzel: ' + kuerz + ' Wert: ' + wert);

                var tempObj = {"wert": wert, "kuerzel": kuerz};
                //console.log(tempObj)
                list.push(tempObj);
            }

            //console.log('Keys: ' + Object.keys(this.wallets));
            //console.log('Data: ' + this.wallets.data);
            //console.log('Objekts in Data: ' + this.wallets.data[1].name);
            return list;
        },

        toShowWallets() {

            var wallList = [];
            var wallets = this.$props.wallets;
            var sizeOfWallets = wallets.length;
            console.log('Anzahl Wallets: ' + sizeOfWallets)
            for (var c = 0; c < sizeOfWallets; c++) {
                var name = wallets[c].name;
                var tmpWallObj = {"name": name}
                wallList.push(tmpWallObj);
            }

            return wallList;
        },

        buy: async function(){

            let amount = document.getElementById("amount").value;
            let currentObj = document.getElementById("currency").value;
            let walletSel = document.getElementById("wallet").value;
            let currencyValue = parseFloat(currentObj);
            let ganzObj = currentObj.split(',');
            console.log('Ganzes Objekt ' + ganzObj)
            let waehrung = ganzObj[1];
            let date = this.getDate();
            console.log(' Current Date: ' + date);
            console.log(this.$props.wallets.length);
            for (var d = 0; d < this.$props.wallets.length; d++) {
                console.log(walletSel);

                if (this.$props.wallets[d].name === walletSel) {

                    if (this.$props.wallets[d].currency === waehrung) {

                        let walletID = this.$props.wallets[d].id;
                        console.log('WalletID: ' + walletID);
                        console.log('Purchase gestattet! ');

                        let purchase = {
                            "date": date,
                            "amount": amount,
                            "price": currencyValue,
                            "currency": waehrung
                        };

                        console.log(purchase);

                        //link flo http://localhost:8080/php43/php43_cryptoapp/server/api/purchase
                        this.$emit(addToWallet,purchase);

                    } else {
                        alert('Current Purchase Not Posible!');
                    }
                }
            }
            console.log('Waehrung ' + waehrung + ' Pro Waehrung: ' + currencyValue + ' Amount ' + amount + ' Wallet: ' + walletSel);
        },

        getDate() {
            Number.prototype.padLeft = function (base, chr) {
                var len = (String(base || 10).length - String(this).length) + 1;
                return len > 0 ? new Array(len).join(chr || '0') + this : this;
            }
            var d = new Date,
                dformat = [d.getFullYear(),
                        (d.getMonth() + 1).padLeft(),
                        d.getDate().padLeft(),
                    ].join('-') + ' ' +
                    [d.getHours().padLeft(),
                        d.getMinutes().padLeft(),
                        d.getSeconds().padLeft()].join(':');
            return dformat;
        },

    },
    computed: {}
})